mkfile_path := $(abspath $(lastword $(MAKEFILE_LIST)))
dir_path := $(dir $(mkfile_path))

install:
	@echo installing executables to ${HOME}/.local/bin
	@mkdir -p ${HOME}/.local/bin
	@ln -s ${dir_path}/bin/organize_photo_by_cdate ${HOME}/.local/bin
	@ln -s ${dir_path}/bin/find_folder_by_datename_before ${HOME}/.local/bin
	@ln -s ${dir_path}/bin/find_folder_by_datename_after ${HOME}/.local/bin
	@ln -s ${dir_path}/bin/exec_cmd_on_folder_by_datename_before ${HOME}/.local/bin
	@ln -s ${dir_path}/bin/exec_cmd_on_folder_by_datename_after ${HOME}/.local/bin
	./install_deps

uninstall:
	@echo removing executables to ${HOME}/.local/bin
	@rm -f ${HOME}/.local/bin/organize_photo_by_cdate
	@rm -f ${HOME}/.local/bin/find_folder_by_datename_before
	@rm -f ${HOME}/.local/bin/find_folder_by_datename_after
	@rm -f ${HOME}/.local/bin/exec_cmd_on_folder_by_datename_before
	@rm -f ${HOME}/.local/bin/exec_cmd_on_folder_by_datename_after

.PHONY: install uninstall

